import json
import ujson
import time
import os
import codecs
import Queue
from datetime import datetime
from elasticsearch import Elasticsearch
from json import JSONDecoder
from functools import partial
from thread import start_new_thread
from threading import Thread
import sys

print ('GET test is started')

#variables
#esHostIp = '10.10.100.56'
#dataFolder = "/tmp/mdb.json"
#queryWith = 'name'
#thCount = 10


workerState = True
q = Queue.Queue(3000000)

def worker():
    print("worker init")
    es = Elasticsearch(host=esHostIp)
    while workerState or not q.empty():
        try:
            if q.empty() == False:
                item = q.get() 
                result = es.search(
                    index=indexName,
                    body={
                        'query':{
                            'match':{
                                'name':{
                                    'query':item,
                                    'operator':"and"
                                }, 
                            },
                        },
                        "fields" : [ "name" ]
                    },
                )                
                print ("Waiting search: {0} , hits of result:{1}".format(q.qsize(),result['hits']['total']))
                q.task_done()
            else:
                print("Waiting")
                time.sleep(1)
        except Exception as e:
            print (e)
            raise
    print("Thread finished")
     
def parser():
    start_time = time.time()
    decoder=JSONDecoder()
    json = []
    num = 0
    with open(dataPath, "r") as f:
        for line in f:
            num = num +1
            if "}," in line:
                json.append("}")
                last = '\n'.join(json)
                json = []
                if last[0] == "[":
                    last = last[1:]
                
                last = "[{0}]".format(last)
                result, index = decoder.raw_decode(last)
                q.put(result[0][queryWith])                
            else:
                json.append(line)
    workerState = False
    while not q.empty():
        time.sleep(1)
    elapsed_time = time.time() - start_time
    print("elapsed_time:{0}".format(elapsed_time))

def query():         
    for i in range(thCount):        
        t = Thread(target=worker)
        t.daemon = True
        t.start()

def main():
    global esHostIp
    esHostIp = sys.argv[1]
    global dataPath
    dataPath = sys.argv[2]
    global indexName
    indexName = sys.argv[3]
    global queryWith
    queryWith = sys.argv[4]
    global thCount
    thCount = int(sys.argv[5])    
    query()
    parser()    
    

if __name__ == '__main__':
    main()

