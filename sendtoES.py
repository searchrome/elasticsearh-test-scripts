import json
import ujson
import time
import os
import codecs
import Queue
from datetime import datetime
from elasticsearch import Elasticsearch
from json import JSONDecoder
from functools import partial
from threading import Thread
import sys

print ('starting')
workerState = True
q = Queue.Queue(3000000)
thCount = 0

def worker():
    es = Elasticsearch(host=esHostIp)
    while workerState or not q.empty():
        try:
            if q.empty() == False:                
                item = q.get()
                es.index(index=indexName, doc_type="test-type", body=item)        
                q.task_done()
                print (q.qsize())
            else:
                time.sleep(1)
        except:
            print (e)
            raise

def parser():
    decoder=JSONDecoder()
    json = []
    num = 0
    with open(dataFolder, "r") as f:
        for line in f:
            num = num +1
            if "}," in line:
                json.append("}")
                last = '\n'.join(json)
                if last[0] == "[":
                    last = last[1:]                
                q.put(last)
                json = []
            else:
                json.append(line)
    workerState = False
    while not q.empty():
        time.sleep(1)

def send():
    for i in range(thCount):
        t = Thread(target=worker)
        t.daemon = True
        t.start()

def main():
    global esHostIp
    esHostIp = sys.argv[1]
    global dataFolder 
    dataFolder = sys.argv[2]
    global indexName
    indexName = sys.argv[3]
    global thCount
    thCount = int(sys.argv[4])
    print(thCount)
    send()
    parser()    


if __name__ == '__main__':
    main()

